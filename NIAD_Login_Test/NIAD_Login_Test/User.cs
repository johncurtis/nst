﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NIAD_Login_Test
{
    public class User
    {
        private string username;
        private string password;

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public User ()
        {
            Username = null;
            Password = null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// Database is created and stored under the APP_DATA folder.
// Using basic connection method for this, as Entity or ADO would be too much

namespace NIAD_Login_Test
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            // Representation of our User

            User user1 = new NIAD_Login_Test.User();

            // Very basic validation that could probably otherwise be made more thorough
            if (tbUsername.Text.Length > 0 && tbPassword.Text.Length > 0)
            {
                user1.Username = tbUsername.Text;
                user1.Password = tbPassword.Text;
                LoginUser(user1);
            }
            else
            {
                lblLogin.Text = "Something went wrong, please ensure both fields have been filled in.";
            }
        }

        private void LoginUser(User user)
        {
            // Connection string that should likely work
            // On any host machine

            string sqlConnectionString =
                @"Data Source=(LocalDB)\MSSQLLocalDB;" +
                @"AttachDbFilename =|DataDirectory|\Database.mdf; 
                Integrated Security = True;";

            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                // Open the connection once the connection object has been instantiated 
                connection.Open();

                // Define the command that we're going to be using 
                SqlCommand cmd = new SqlCommand("LoginUser", connection);

                // Identify the type of query to be executed. Procedure won't execute correctly as a traditional "SELECT"
                cmd.CommandType = CommandType.StoredProcedure;

                // Our lovely parameters, including our output
                // These can be fetched from the user object passed in
                cmd.Parameters.Add(new SqlParameter("@username", user.Username));
                cmd.Parameters.Add(new SqlParameter("@password", user.Password));

                // This is the only way I've found to handle output paramters
                // This could be omitted and instead the reader object can be iterated through 

                SqlParameter output = new SqlParameter("@output", "");
                output.Direction = ParameterDirection.Output;
                output.Size = 255;

                cmd.Parameters.Add(output);

                // Execute the command
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (output.Value.ToString().Length > 0)
                    {
                       lblLogin.Text = String.Format("You have been successfully logged in as {0}!", output.Value.ToString().Trim());
                    }
                    else { lblLogin.Text = "Invalid username or password."; }
                    
                }
            }
        }
    }
}
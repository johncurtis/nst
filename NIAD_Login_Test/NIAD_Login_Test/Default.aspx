﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NIAD_Login_Test.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="lblInformation" runat="server" Text="This is a simple project used to fulfil the requirements outlined in the NIAD Systems email assessment."></asp:Label>
    
        <br />
        <table style="width:100%;">
            <tr>
                <td>
                    <asp:Label ID="lblUsername" runat="server" Text="Username"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbUsername" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLogin" runat="server"></asp:Label>
                </td>
                <td>

                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" />

                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
